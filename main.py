import argparse
import loguru
import pprint
from scapy.all import *

def check_weird_parameters(params):
    """Check whether the PCAP has a lost of parameters set."""
    if len(params) >= 8:
        loguru.logger.error("This packet has a lot of DHCP parameters set. Maybe take a closer look.")

def get_message_type(type_int):
    """What kind of DHCP message is this?"""
    message_type = str()
    if type_int == 1:
        message_type = "DHCPDISCOVER"
    if type_int == 2:
        message_type = "DHCPOFFER"
    if type_int == 3:
        message_type = "DHCPREQUEST"
    if type_int == 4:
        message_type = "DHCPDECLINE"
    if type_int == 5:
        message_type = "DHCPACK"
    if type_int == 6:
        message_type = "DHCPNAK"
    if type_int == 7:
        message_type = "DHCPRELEASE"
    if type_int == 8:
        message_type = "DHCPINFORM"
    if type_int == 9:
        message_type = "DHCPFORCERENEW"
    if type_int == 10:
        message_type = "DHCPLEASEQUERY"
    if type_int == 11:
        message_type = "DHCPLEASEUNASSIGNED"
    if type_int == 12:
        message_type = "DHCPLEASEUNKNOWN"
    if type_int == 13:
        message_type = "DHCPLEASEACTIVE"
    if type_int == 14:
        message_type = "DHCPBULKLEASEQUERY"
        loguru.logger.error("A bulk lease query has occurred. Was this expected?")
    if type_int == 15:
        message_type = "DHCPLEASEQUERYDONE"
    if type_int == 16:
        message_type = "DHCPACTIVELEASEQUERY"
    if type_int == 17:
        message_type = "DHCPLEASEQUERYSTATUS"
    if type_int == 18:
        message_type = "DHCPTLS"
    return message_type

def process_param_req_list(param_req_list):
    parameters = list()
    for i in param_req_list:
        if i == 1:
            parameters.append("subnet mask")
        if i == 2:
            parameters.append("Time offset")
        if i == 3:
            parameters.append("router")
        if i == 4:
            parameters.append("Time server")
        if i == 6:
            parameters.append("DNS server")
        if i == 42:
            parameters.append("NTP servers")
    return parameters


def summarize_packets(total_packets, dhcp_packets):
    """Summarize the packets."""
    try:
        pct_dhcp = "{0:.0%}".format(dhcp_packets/total_packets)
    except:
        loguru.logger.error("Sorry. We had a math problem")
    a_summary = "Neat. " + pct_dhcp + " of the packets examined were DHCP packets."
    return a_summary


def check_lease_time(lease_time):
    """Check the duration of the DHCP lease."""
    if lease_time <= 3599:
        loguru.logger.info("Kind of a short lease. Less than an hour.")
    if lease_time == 3600:
        loguru.logger.info("Cool. A one hour lease has been requested.")
    if 3601 <= lease_time <= 86399:
        loguru.logger.info("Kind of a weird lease. More than an hour, less than a day.")
    if lease_time == 86400:
        loguru.logger.info("Cool. A one day lease has been requested.")
    if lease_time < 86400:
        loguru.logger.info("What a long lease you have.")


def main(pcap_file):
    try:
        packets = rdpcap(pcap_file)
    except:
        loguru.logger.error("Sorry, could not load the pcap.")

    no_thanks = list()
    no_thanks = ['end', 'pad']
    lease_time = "lease_time"
    msg_type = str()
    msg_type = "message-type"
    client_fqdn = "client_FQDN"
    p_r_l = "param_req_list"
    dhcp_packet_number = 0
    packet_number = 0
    for packet in packets:
        packet_number = packet_number + 1
        # Put the stuff in this loop in to a function
        if DHCP in packet:
            loguru.logger.info("DHCP Packet #{packet_number}", packet_number=dhcp_packet_number)
            dhcp_packet_number = dhcp_packet_number + 1
            dhcp_options = packet[DHCP].options
            for opt in dhcp_options:
                if type(opt) == tuple:
                    if lease_time in opt:
                        loguru.logger.info("What kind of lease do you want?")
                        check_lease_time(opt[1])
                    elif msg_type in opt:
                        opt_message_type = opt[1]
                        message_type = get_message_type(opt_message_type)
                        loguru.logger.info("DHCP Packet Type: {message_type}", message_type=message_type)
                    elif p_r_l in opt:
                        requested_parameters = opt[1]
                        packet_parameters = process_param_req_list(requested_parameters)
                        check_weird_parameters(packet_parameters)
                        loguru.logger.debug("Requested Parameters: {packet_parameters}", packet_parameters=packet_parameters)
                    elif client_fqdn in opt:
                        encoding = 'utf-8'
                        the_client_fqdn = opt[1].decode(encoding)
                        loguru.logger.debug("Client FQDN: {the_client_fqdn}", the_client_fqdn=the_client_fqdn)
                    else:
                        loguru.logger.info(opt)
    summary = summarize_packets(packet_number, dhcp_packet_number)
    loguru.logger.success(summary)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""DHCP Party // This is a Python program that will parse a specified PCAP and provide detailed information about any DHCP packets that it finds.""")
    parser.add_argument('--pcap',  type=str, default="dhcp-sample.pcap", help="The path to a PCAP file.")
    args = parser.parse_args()
    if len(sys.argv) == 1:
      parser.print_help()
      loguru.logger.info("Try passing --pcap and a PCAP file.")
      sys.exit()
    pcap_file = args.pcap
    main(pcap_file)
